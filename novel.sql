/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : novel

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 02/11/2022 10:42:17
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for advert
-- ----------------------------
DROP TABLE IF EXISTS `advert`;
CREATE TABLE `advert`  (
  `advert_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '广告ID',
  `advert_picture` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '广告图片路径',
  `status` int(1) NULL DEFAULT 0 COMMENT '广告是否展出状态(0代表不展出，1代表展出)',
  `is_deleted` int(1) NULL DEFAULT 0 COMMENT '逻辑删除',
  `create_time` timestamp(0) NULL DEFAULT NULL COMMENT '创建字段时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改字段时间',
  PRIMARY KEY (`advert_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of advert
-- ----------------------------
INSERT INTO `advert` VALUES ('48e7156d0dc5a2064e6f19c63dcc9250', 'https://images.pexels.com/photos/957024/forest-trees-perspective-bright-957024.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1', 0, 0, '2022-10-24 21:06:56', '2022-10-31 10:23:33');
INSERT INTO `advert` VALUES ('cd839f410a505d490f52cc4de8c2b6e5', 'https://images.pexels.com/photos/33109/fall-autumn-red-season.jpg?auto=compress&cs=tinysrgb&w=1600', 0, 0, '2022-10-24 21:07:17', '2022-10-31 10:23:37');
INSERT INTO `advert` VALUES ('e1b2349f9d4ef15644f45d143da1df3e', 'https://images.pexels.com/photos/1459534/pexels-photo-1459534.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1', 1, 0, '2022-10-24 21:11:08', '2022-10-24 21:11:08');
INSERT INTO `advert` VALUES ('e6b9a5a4a0202e8319ffbd420589e01d', 'https://images.pexels.com/photos/70365/forest-sunbeams-trees-sunlight-70365.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1', 1, 0, '2022-10-24 21:10:39', '2022-10-24 21:10:39');

-- ----------------------------
-- Table structure for chapter
-- ----------------------------
DROP TABLE IF EXISTS `chapter`;
CREATE TABLE `chapter`  (
  `chapter_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '章节ID',
  `chapter_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '章节名称 ',
  `chapter_content` text CHARACTER SET utf8 COLLATE utf8_bin NULL COMMENT '具体写在章节里面的内容(文字)',
  `is_deleted` int(1) NULL DEFAULT 0 COMMENT '逻辑删除(0代表未删除，1代表已删除)',
  `create_time` timestamp(0) NULL DEFAULT NULL COMMENT '字段创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '字段修改时间',
  `novel_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '小说Id',
  `chapter_index` int(10) NULL DEFAULT 0 COMMENT '章节序号',
  PRIMARY KEY (`chapter_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of chapter
-- ----------------------------
INSERT INTO `chapter` VALUES ('099b9b0d4a59324b5c37410ef9cc1ee6', '火焰山历险记', NULL, 1, '2022-10-30 17:36:10', '2022-10-30 20:43:47', 'd65e3b18dd8789c80ccadf238b812529', 0);
INSERT INTO `chapter` VALUES ('1a7a0f8b7cde4899b7c0ad898d5f355c', '官封弼马心何足　名注齐天意未宁', NULL, 0, '2022-10-31 08:09:46', '2022-10-31 08:09:46', 'd65e3b18dd8789c80ccadf238b812529', 0);
INSERT INTO `chapter` VALUES ('1c656464b7bf00f5be7be439e3d6b2ce', '孙悟空三岛求方　观世音甘泉活树', NULL, 0, '2022-10-31 08:12:18', '2022-10-31 08:12:18', 'd65e3b18dd8789c80ccadf238b812529', 0);
INSERT INTO `chapter` VALUES ('1d698428a9ce3fcc39c0b8de75d24626', '护法设庄留大圣　须弥灵吉定风魔', NULL, 0, '2022-10-31 08:11:53', '2022-10-31 08:11:53', 'd65e3b18dd8789c80ccadf238b812529', 0);
INSERT INTO `chapter` VALUES ('2563463e2ec6ea97cfc76b3aaddca58f', '外道迷真性　元神助本心', '大家看好大口径的哈哈都i啊活动i啊电话啊大大大', 0, '2022-10-31 08:12:57', '2022-10-31 08:12:57', 'd65e3b18dd8789c80ccadf238b812529', 0);
INSERT INTO `chapter` VALUES ('2787dbb13a6b2b98ad4e5a9d15be9fd2', '脱难江流来国土　承恩八戒转山林', NULL, 0, '2022-10-31 08:12:36', '2022-10-31 08:12:36', 'd65e3b18dd8789c80ccadf238b812529', 0);
INSERT INTO `chapter` VALUES ('30c58404d83168978beaf53d775fcde7', '蛇盘山诸神暗佑　鹰愁涧意马收缰', NULL, 0, '2022-10-31 08:10:47', '2022-10-31 08:10:47', 'd65e3b18dd8789c80ccadf238b812529', 0);
INSERT INTO `chapter` VALUES ('35f38add8143e08ef50782287ac4c998', '悟彻菩提真妙理　断魔归本合元神', '唐僧的爱情故事阿瓦低洼低洼低洼低洼的哇大王发方式发', 0, '2022-10-25 17:22:12', '2022-10-30 20:10:56', 'd65e3b18dd8789c80ccadf238b812529', 0);
INSERT INTO `chapter` VALUES ('373509f81b13f1e6466e647be45d6012', '袁守诚妙算无私曲　老龙王拙计犯天条', NULL, 0, '2022-10-31 08:10:17', '2022-10-31 08:10:17', 'd65e3b18dd8789c80ccadf238b812529', 0);
INSERT INTO `chapter` VALUES ('37f1b2a52e1a206cb4720f0f6102ebf4', '玄奘秉诚建大会　观音显象化金蝉', NULL, 0, '2022-10-31 08:10:32', '2022-10-31 08:10:32', 'd65e3b18dd8789c80ccadf238b812529', 0);
INSERT INTO `chapter` VALUES ('3ae2fbf872fab09375da94b75f50ae92', '见鬼2法', '室内打伞', 0, '2022-10-30 20:48:37', '2022-10-30 20:48:37', '758b6e996824e0e3c518820c934dc8ab', 0);
INSERT INTO `chapter` VALUES ('46a048f6e7a56930a110d4a8c8c5c55e', '镇元仙赶捉取经僧　孙行者大闹五庄观', NULL, 0, '2022-10-31 08:12:13', '2022-10-31 08:12:13', 'd65e3b18dd8789c80ccadf238b812529', 0);
INSERT INTO `chapter` VALUES ('5364ef4f1ca3b9797c78beebf21f3b1e', '八戒大战流沙河　木叉奉法收悟净', NULL, 0, '2022-10-31 08:11:57', '2022-10-31 08:11:57', 'd65e3b18dd8789c80ccadf238b812529', 0);
INSERT INTO `chapter` VALUES ('54ae4c871e34043df9c4085c1521bfc9', '花果山群妖聚义　黑松林三藏逢魔', NULL, 0, '2022-10-31 08:12:29', '2022-10-31 08:12:29', 'd65e3b18dd8789c80ccadf238b812529', 0);
INSERT INTO `chapter` VALUES ('63b8505da8fea6b1042e0fe19099be36', '二将军宫门镇鬼　唐太宗地府还魂', NULL, 0, '2022-10-31 08:10:22', '2022-10-31 08:10:22', 'd65e3b18dd8789c80ccadf238b812529', 0);
INSERT INTO `chapter` VALUES ('66a6df4cf78d70a05b0a30feb35c0e1a', '八卦炉中逃大圣　五行山下定心猿', NULL, 0, '2022-10-31 08:10:05', '2022-10-31 08:10:05', 'd65e3b18dd8789c80ccadf238b812529', 0);
INSERT INTO `chapter` VALUES ('712c2dbb8ebc4353a17937ea165833ae', '我佛造经传极乐　观音奉旨上长安', NULL, 0, '2022-10-31 08:10:11', '2022-10-31 08:10:11', 'd65e3b18dd8789c80ccadf238b812529', 0);
INSERT INTO `chapter` VALUES ('8455e30470b5b4ddeb5d12bf2e4e0611', '还受生唐王遵善果　度孤魂萧瑀正空门', NULL, 0, '2022-10-31 08:10:28', '2022-10-31 08:10:28', 'd65e3b18dd8789c80ccadf238b812529', 0);
INSERT INTO `chapter` VALUES ('89ad876210245158712fd3027c0a529b', '洪太尉误放魔君', '108位魔君@散', 0, '2022-10-28 15:18:55', '2022-10-28 15:18:55', '7a59814935fee4096f30ed29567010b0', 0);
INSERT INTO `chapter` VALUES ('8c26811b25873220048a2881344ba72d', '观音院僧谋宝贝　黑风山怪窃袈裟', NULL, 0, '2022-10-31 08:10:52', '2022-10-31 08:10:52', 'd65e3b18dd8789c80ccadf238b812529', 0);
INSERT INTO `chapter` VALUES ('8c6919f47b7f0f01cadd7be073307419', '乱蟠桃大圣偷丹　反天宫诸神捉怪', NULL, 0, '2022-10-31 08:09:52', '2022-10-31 08:09:52', 'd65e3b18dd8789c80ccadf238b812529', 0);
INSERT INTO `chapter` VALUES ('8e451c42d7fac7a776275ab1fa424bed', '心猿归正　六贼无踪', NULL, 0, '2022-10-31 08:10:42', '2022-10-31 08:10:42', 'd65e3b18dd8789c80ccadf238b812529', 0);
INSERT INTO `chapter` VALUES ('8eb4de2a3569f1897fb5fa3116179db7', '尸魔三戏唐三藏　圣僧恨逐美猴王', NULL, 0, '2022-10-31 08:12:24', '2022-10-31 08:12:24', 'd65e3b18dd8789c80ccadf238b812529', 0);
INSERT INTO `chapter` VALUES ('92f8c5a5eeb119f6125dfb730e32485f', '平顶山功曹传信　莲花洞木母逢灾', NULL, 0, '2022-10-31 08:12:52', '2022-10-31 08:12:52', 'd65e3b18dd8789c80ccadf238b812529', 0);
INSERT INTO `chapter` VALUES ('aa5c10bbdad2d02534f70b26393651f5', '猪八戒义激猴王　孙行者智降妖怪', NULL, 0, '2022-10-31 08:12:47', '2022-10-31 08:12:47', 'd65e3b18dd8789c80ccadf238b812529', 0);
INSERT INTO `chapter` VALUES ('aaad1adf0d28a20d423723781f9be167', '三藏不忘本　四圣试禅心', NULL, 0, '2022-10-31 08:12:03', '2022-10-31 08:12:03', 'd65e3b18dd8789c80ccadf238b812529', 0);
INSERT INTO `chapter` VALUES ('acdd41279e2dd7d13c5bac23e8d84596', '云栈洞悟空收八戒　浮屠山玄奘受心经', NULL, 0, '2022-10-31 08:11:43', '2022-10-31 08:11:43', 'd65e3b18dd8789c80ccadf238b812529', 0);
INSERT INTO `chapter` VALUES ('afea129c481d37b1ef59f5378c15cfa1', '陷虎穴金星解厄　双叉岭伯钦留僧', NULL, 0, '2022-10-31 08:10:38', '2022-10-31 08:10:38', 'd65e3b18dd8789c80ccadf238b812529', 0);
INSERT INTO `chapter` VALUES ('b7d7e07177dd1e206212f5ea3d7d9f84', '观音赴会问原因　小圣施威降大圣', NULL, 0, '2022-10-31 08:09:59', '2022-10-31 08:09:59', 'd65e3b18dd8789c80ccadf238b812529', 0);
INSERT INTO `chapter` VALUES ('bed5537893922c67ea96338722faed86', '万寿山大仙留故友　五庄观行者窃人参', NULL, 0, '2022-10-31 08:12:08', '2022-10-31 08:12:08', 'd65e3b18dd8789c80ccadf238b812529', 0);
INSERT INTO `chapter` VALUES ('c9cdef6dd7cb1ea790d43c0b14f52d4d', '四海千山皆拱伏　九幽十类尽除名', NULL, 0, '2022-10-31 08:09:39', '2022-10-31 08:09:39', 'd65e3b18dd8789c80ccadf238b812529', 0);
INSERT INTO `chapter` VALUES ('d4921c8037290bbe32eb9f723fb38e0f', '灵根育孕源流出　心性修持大道生', '混沌未分天地乱，茫茫渺渺无人见。自从盘古破鸿蒙，开辟从兹清浊辨。\n\n覆载群生仰至仁，发明万物皆成善。欲知造化会元功，须看西游释厄传。\n\n盖闻天地之数，有十二万九千六百岁为一元。将一元分为十二会，乃子、丑、寅、卯、辰、巳、午、未、申、酉、戌、亥之十二支也。每会该一万八百岁。且就一日而论：子时得阳气，而丑则鸡鸣；寅不通光，而卯则日出；辰时食后，而巳则挨排；日午天中，而未则西蹉；申时晡而日落酉，戌黄昏而人定亥。譬于大数，若到戌会之终，则天地昏缯而万物否矣。\n\n再去五千四百岁，交亥会之初，则当黑暗，而两间人物俱无矣，故曰混沌。又五千四百岁，亥会将终，贞下起元，近子之会，而复逐渐开明。邵康节曰：“冬至子之半，天心无改移。一阳初动处，万物未生时。”到此天始有根。\n\n再五千四百岁，正当子会，轻清上腾，有日有月有星有辰。日月星辰，谓之四象。故曰天开于子。又经五千四百岁，子会将终，近丑之会，而逐渐坚实。《易》曰：“大哉乾元！至哉坤元！万物资生，乃顺承天。”至此，地始凝结。\n\n再五千四百岁，正当丑会，重浊下凝，有水有火有山有石有土。水火山石土，谓之五形。故曰地辟于丑。又经五千四百岁，丑会终而寅会之初，发生万物。历曰：“天气下降，地气上升；天地交合，群物皆生。”至此，天清地爽，阴阳交合。\n\n再五千四百岁，正当寅会，生人生兽生禽，正谓天地人，三才定位。故曰人生于寅。\n\n感盘古开辟，三皇治世，五帝定伦，世界之间，遂分为四大部洲：曰东胜神洲，曰西牛贺洲，曰南赡部洲，曰北俱芦洲。这部书单表东胜神洲。海外有一国土，名曰傲来国。国近大海，海中有一座名山，唤为花果山。此山乃十洲之祖脉，三岛之来龙，自开清浊而立，鸿蒙判后而成。真个好山！有词赋为证，赋曰：\n\n势镇汪洋，威宁瑶海。势镇汪洋，潮涌银山鱼入穴；威宁瑶海，波翻雪浪蜃离渊。水火方隅高积土，东海之处耸崇巅。丹崖怪石，削壁奇峰。丹崖上，彩凤双鸣；削壁前，麒麟独卧。峰头时听锦鸡鸣，石窟每观龙出入。林中有寿鹿仙狐，树上有灵禽玄鹤。瑶草奇花不谢，青松翠柏长春。仙桃常结果，修竹每留云。一条涧壑藤萝密，四面原堤草色新。正是百川会处擎天柱，万劫无移大地根。\n\n那座山正当顶上，有一块仙石。其石有三丈六尺五寸高，有二丈四尺围圆。三丈六尺五寸高，按周天三百六十五度；二丈四尺围圆，按政历二十四气。上有九窍八孔，按九宫八卦。四面更无树木遮阴，左右倒有芝兰相衬。盖自开辟以来，每受天真地秀，日精月华，感之既久，遂有灵通之意。内育仙胞。一日迸裂，产一石卵，似圆球样大。因见风，化作一个石猴。五官俱备，四肢皆全。便就学爬学走，拜了四方。目运两道金光，射冲斗府。惊动高天上圣大慈仁者玉皇大天尊玄穹高上帝，驾座金阙云宫灵霄宝殿，聚集仙卿，见有金光焰焰，即命千里眼、顺风耳开南天门观看。二将果奉旨出门外，看的真，听的明。\n\n须臾回报道：“臣奉旨观听金光之处，乃东胜神洲海东傲来小国之界，有一座花果山，山上有一仙石，石产一卵，见风化一石猴，在那里拜四方，眼运金光，射冲斗府。如今服饵水食，金光将潜息矣。”玉帝垂赐恩慈曰：“下方之物，乃天地精华所生，不足为异。”\n\n那猴在山中，却会行走跳跃，食草木，饮涧泉，采山花，觅树果；与狼虫为伴，虎豹为群，獐鹿为友，猕猿为亲；夜宿石崖之下，朝游峰洞之中。真是“山中无甲子，寒尽不知年”。一朝天气炎热，与群猴避暑，都在松阴之下顽耍。你看他一个个——\n\n跳树攀枝，采花觅果；抛弹子，邷么儿，跑沙窝，砌宝塔；赶蜻蜓，扑八蜡；参老天，拜菩萨；扯葛藤，编草未；捉虱子，咬圪蚤；理毛衣，剔指甲；挨的挨，擦的擦；推的推，压的压；扯的扯，拉的拉，青松林下任他顽，绿水涧边随洗濯。\n\n一群猴子耍了一会，却去那山涧中洗澡。见那股涧水奔流，真个似滚瓜涌溅。古云：禽有禽言，兽有兽语。众猴都道：“这股水不知是那里的水。我们今日赶闲无事，顺涧边往上溜头寻看源流，耍子去耶！”喊一声，都拖男挈女，唤弟呼兄，一齐跑来，顺涧爬山，直至源流之处，乃是一股瀑布飞泉。但见那——\n\n一派白虹起，千寻雪浪飞。海风吹不断，江月照还依。\n\n冷气分青嶂，余流润翠微。潺蔽名瀑布，真似挂帘帷。\n\n众猴拍手称扬道：“好水，好水！原来此处远通山脚之下，直接大海之波。”又道：“那一个有本事的，钻进去寻个源头出来不伤身体者，我等即拜他为王。”连呼了三声，忽见丛杂中跳出一个石猴，应声高叫道：“我进去，我进去！”好猴！也是他——\n\n今日芳名显，时来大运通。有缘居此地，天遣入仙宫。\n\n你看他瞑目蹲身，将身一纵，径跳入瀑布泉中，忽睁睛抬头观看，那里边却无水无波，明明朗朗的一架桥梁。他住了身，定了神，仔细再看，原来是座铁板桥，桥下之水，冲贯于石窍之间，倒挂流出去，遮闭了桥门。却又欠身上桥头，再走再看，却似有人家住处一般，真个好所在。但见那——\n\n翠藓堆蓝，白云浮玉，光摇片片烟霞。虚窗静室，滑凳板生花。乳窟龙珠倚挂，萦回满地奇葩。锅灶傍崖存火迹，樽罍靠案见肴渣。石座石床真可爱，石盆石碗更堪夸。又见那一竿两竿修竹，三点五点梅花。几树青松常带雨，浑然象个人家。\n\n看罢多时，跳过桥中间，左右观看，只见正当中有一石碣。碣上有一行楷书大字，镌着“花果山福地，水帘洞洞天”。石猿喜不自胜，急抽身往外便走，复瞑目蹲身，跳出水外，打了两个呵呵道：“大造化，大造化！”众猴把他围住问道：“里面怎么样？水有多深？”石猴道：“没水，没水！原来是一座铁板桥。桥那边是一座天造地设的家当。”众猴道：“怎见得是个家当？”石猴笑道：“这股水乃是桥下冲贯石窍，倒挂下来遮闭门户的。桥边有花有树，乃是一座石房。房内有石锅石灶、石碗石盆、石床石凳，中间一块石碣上，镌着‘花果山福地，水帘洞洞天’。真个是我们安身之处。里面且是宽阔，容得千百口老小。我们都进去住，也省得受老天之气。这里边——\n\n刮风有处躲，下雨好存身。霜雪全无惧，雷声永不闻。\n\n烟霞常照耀，祥瑞每蒸熏。松竹年年秀，奇花日日新。”\n\n众猴听得，个个欢喜。都道：“你还先走，带我们进去，进去！”石猴却又瞑目蹲身，往里一跳，叫道：“都随我进来，进来！”那些猴有胆大的，都跳进去了；胆小的，一个个伸头缩颈，抓耳挠腮，大声叫喊，缠一会，也都进去了。跳过桥头，一个个抢盆夺碗，占灶争床，搬过来，移过去，正是猴性顽劣，再无一个宁时，只搬得力倦神疲方止。石猿端坐上面道：“列位呵，人而无信，不知其可。你们才说有本事进得来，出得去，不伤身体者，就拜他为王。我如今进来又出去，出去又进来，寻了这一个洞天与列位安眠稳睡，各享成家之福，何不拜我为王？”众猴听说，即拱伏无违，一个个序齿排班，朝上礼拜，都称“千岁大王”。自此，石猿高登王位，将“石”字儿隐了，遂称美猴王。有诗为证，诗曰：\n\n三阳交泰产群生，仙石胞含日月精。借卵化猴完大道，假他名姓配丹成。\n\n内观不识因无相，外合明知作有形。历代人人皆属此，称王称圣任纵横。\n\n美猴王领一群猿猴、猕猴、马猴等，分派了君臣佐使，朝游花果山，暮宿水帘洞，合契同情，不入飞鸟之丛，不从走兽之类，独自为王，不胜欢乐。是以——\n\n春采百花为饮食，夏寻诸果作生涯。秋收芋栗延时节，冬觅黄精度岁华。\n\n美猴王享乐天真，何期有三五百载。一日，与群猴喜宴之间，忽然忧恼，堕下泪来。众猴慌忙罗拜道：“大王何为烦恼？”猴王道：“我虽在欢喜之时，却有一点儿远虑，故此烦恼。”众猴又笑道：“大王好不知足！我等日日欢会，在仙山福地，古洞神洲，不伏麒麟辖，不伏凤凰管，又不伏人王拘束，自由自在，乃无量之福，为何远虑而忧也？”猴王道：“今日虽不归人王法律，不惧禽兽威服，将来年老血衰，暗中有阎王老子管着，一旦身亡，可不枉生世界之中，不得久注天人之内？”\n\n众猴闻此言，一个个掩面悲啼，俱以无常为虑。只见那班部中，忽跳出一个通背猿猴，厉声高叫道：“大王若是这般远虑，真所谓道心开发也！如今五虫之内，惟有三等名色，不伏阎王老子所管。”猴王道：“你知那三等人？”猿猴道：“乃是佛与仙与神圣三者，躲过轮回，不生不灭，与天地山川齐寿。”猴王道：“此三者居于何所？”猿猴道：“他只在阎浮世界之中，古洞仙山之内。”猴王闻之，满心欢喜道：“我明日就辞汝等下山，云游海角，远涉天涯，务必访此三者，学一个不老长生，常躲过阎君之难。”噫！这句话，顿教跳出轮回网，致使齐天大圣成。众猴鼓掌称扬，都道：“善哉，善哉！我等明日越岭登山，广寻些果品，大设筵宴送大王也。”\n\n次日，众猴果去采仙桃，摘异果，刨山药，抃黄精，芝兰香蕙，瑶草奇花，般般件件，整整齐齐，摆开石凳石桌，排列仙酒仙肴。但见那——\n\n金丸珠弹，红绽黄肥。金丸珠弹腊樱桃，色真甘美；红绽黄肥熟梅子，味果香酸。鲜龙眼，肉甜皮薄；火荔枝，核小囊红。林檎碧实连枝献，枇杷缃苞带叶擎。兔头梨子鸡心枣，消渴除烦更解酲。香桃烂杏，美甘甘似玉液琼浆；脆李杨梅，酸荫荫如脂酥膏酪。红囊黑子熟西瓜，四瓣黄皮大柿子。石榴裂破，丹砂粒现火晶珠；芋栗剖开，坚硬肉团金玛瑙。胡桃银杏可传茶，椰子葡萄能做酒。榛松榧柰满盘盛，桔蔗柑橙盈案摆。熟煨山药，烂煮黄精。捣碎茯苓并薏苡，石锅微火漫炊羹。人间纵有珍羞味，怎比山猴乐更宁！\n\n群猴尊美猴王上坐，各依齿肩排于下边，一个个轮流上前奉酒奉花奉果，痛饮了一日。次日，美猴王早起，教：“小的们，替我折些枯松，编作筏子，取个竹竿作篙，收拾些果品之类，我将去也。”果独自登筏，尽力撑开，飘飘荡荡，径向大海波中，趁天风来渡南赡部洲地界。这一去，正是那——\n\n天产仙猴道行隆，离山驾筏趁天风。飘洋过海寻仙道，立志潜心建大功。\n\n有分有缘休俗愿，无忧无虑会元龙。料应必遇知音者，说破源流万法通。\n\n也是他运至时来，自登木筏之后，连日东南风紧，将他送到西北岸前，乃是南赡部洲地界。持篙试水，偶得浅水，弃了筏子，跳上岸来。只见海边有人捕鱼、打雁、诞蛤、淘盐。他走近前，弄个把戏，妆个掞虎，吓得那些人丢筐弃网，四散奔跑。将那跑不动的拿住一个，剥了他的衣裳，也学人穿在身上，摇摇摆摆，穿州过府，在市廛中，学人礼，学人话。朝餐夜宿，一心里访问佛仙神圣之道，觅个长生不老之方。见世人都是为名为利之徒，更无一个为身命者，正是那——\n\n争名夺利几时休？早起迟眠不自由。骑着驴骡思骏马，官居宰相望王侯。\n\n只愁衣食耽劳碌，何怕阎君就取勾。继子荫孙图富贵，更无一个肯回头。\n\n猴王参访仙道，无缘得遇，在于南赡部洲，串长城，游小县，不觉八九年余。忽行至西洋大海，他想着海外必有神仙，独自个依前作筏，又飘过西海，直至西牛贺洲地界。登岸遍访多时，忽见一座高山秀丽，林麓幽深。他也不怕狼虫，不惧虎豹，登山顶上观看。果是好山——\n\n千峰排戟，万仞开屏。日映岚光轻锁翠，雨收黛色冷含青。枯藤缠老树，古渡界幽程。奇花瑞草，修竹乔松。修竹乔松，万载常青欺福地；奇花瑞草，四时不谢赛蓬瀛。幽鸟啼声近，源泉响溜清。重重谷壑芝兰绕，处处蒨崖苔藓生。起伏峦头龙脉好，必有高人隐姓名。\n\n正观看间，忽闻得林深之处有人言语，急忙趋步穿入林中，侧耳而听，原来是歌唱之声，歌曰：\n\n观棋柯烂，伐木丁丁，云边谷口徐行。卖薪沽酒，狂笑自陶情。苍径秋高，对月枕松根，一觉天明。认旧林，登崖过岭，持斧断枯藤。收来成一担，行歌市上，易米三升。更无些子争竞，时价平平。不会机谋巧算，没荣辱，恬淡延生。相逢处，非仙即道，静坐讲《黄庭》。\n\n美猴王听得此言，满心欢喜道：“神仙原来藏在这里！”即忙跳入里面，仔细再看，乃是一个樵子，在那里举斧砍柴，但看他打扮非常——\n\n头上戴箬笠，乃是新笋初脱之箨。身上穿布衣，乃是木绵拈就之纱。腰间系环绦，乃是老蚕口吐之丝。足下踏草履，乃是枯莎槎就之爽。手执钢斧，担挽火麻绳。扳松劈枯树，争似此樵能！\n\n猴王近前叫道：“老神仙，弟子起手！”那樵汉慌忙丢了斧，转身答礼道：“不当人，不当人！我拙汉衣食不全，怎敢当‘神仙’二字？”猴王道：“你不是神仙，如何说出神仙的话来？”樵夫道：“我说什么神仙话？”猴王道：“我才来至林边，只听的你说：‘相逢处，非仙即道，静坐讲《黄庭》。’《黄庭》乃道德真言，非神仙而何？”樵夫笑道：“实不瞒你说，这个词名做《满庭芳》，乃一神仙教我的。那神仙与我舍下相邻。他见我家事劳苦，日常烦恼，教我遇烦恼时，即把这词儿念念，一则散心，二则解困，我才有些不足处思虑，故此念念。不期被你听了。”猴王道：“你家既与神仙相邻，何不从他修行？学得个不老之方，却不是好？”樵夫道：“我一生命苦，自幼蒙父母养育至八九岁，才知人事，不幸父丧，母亲居孀。再无兄弟姊妹，只我一人，没奈何，早晚侍奉。如今母老，一发不敢抛离。却又田园荒芜，衣食不足，只得斫两束柴薪，挑向市廛之间，货几文钱，籴几升米，自炊自造，安排些茶饭，供养老母，所以不能修行。”猴王道：“据你说起来，乃是一个行孝的君子，向后必有好处。但望你指与我那神仙住处，却好拜访去也。”樵夫道：“不远，不远。此山叫做灵台方寸山，山中有座斜月三星洞，那洞中有一个神仙，称名须菩提祖师。那祖师出去的徒弟，也不计其数，见今还有三四十人从他修行。你顺那条小路儿，向南行七八里远近，即是他家了。”猴王用手扯住樵夫道：“老兄，你便同我去去，若还得了好处，决不忘你指引之恩。”樵夫道：“你这汉子，甚不通变。我方才这般与你说了，你还不省？假若我与你去了，却不误了我的生意？老母何人奉养？我要斫柴，你自去，自去。”\n\n猴王听说，只得相辞。出深林，找上路径，过一山坡，约有七八里远，果然望见一座洞府。挺身观看，真好去处。但见——\n\n烟霞散彩，日月摇光。千株老柏，万节修篁。千株老柏，带雨半空青冉冉；万节修篁，含烟一壑色苍苍。门外奇花布锦，桥边瑶草喷香。石崖突兀青苔润，悬壁高张翠藓长。时闻仙鹤唳，每见凤凰翔。仙鹤唳时，声振九皋霄汉远；凤凰翔起，翎毛五色彩云光。玄猿白鹿随隐见，金狮玉象任行藏。细观灵福地，真个赛天堂！\n\n又见那洞门紧闭，静悄悄杳无人迹。忽回头，见崖头立一石碑，约有三丈余高，八尺余阔，上有一行十个大字，乃是“灵台方寸山，斜月三星洞”。美猴王十分欢喜道：“此间人果是朴实，果有此山此洞。”看勾多时，不敢敲门。且去跳上松枝梢头，摘松子吃了顽耍。\n\n少顷间，只听得呀的一声，洞门开处，里面走出一个仙童，真个丰姿英伟，象貌清奇，比寻常俗子不同。但见他——\n\n骛髻双丝绾，宽袍两袖风。貌和身自别，心与相俱空。\n\n物外长年客，山中永寿童。一尘全不染，甲子任翻腾。\n\n那童子出得门来，高叫道：“什么人在此搔扰？”猴王扑的跳下树来，上前躬身道：“仙童，我是个访道学仙之弟子，更不敢在此搔扰。”仙童笑道：“你是个访道的么？”猴王道：“是。”童子道：“我家师父正才下榻登坛讲道，还未说出原由，就教我出来开门，说：‘外面有个修行的来了，可去接待接待。’想必就是你了？”猴王笑道：“是我，是我。”童子道：“你跟我进来。”\n\n这猴王整衣端肃，随童子径入洞天深处观看：一层层深阁琼楼，一进进珠宫贝阙，说不尽那静室幽居。直至瑶台之下，见那菩提祖师端坐在台上，两边有三十个小仙侍立台下。果然是——\n\n大觉金仙没垢姿，西方妙相祖菩提。不生不灭三三行，全气全神万万慈。\n\n空寂自然随变化，真如本性任为之。与天同寿庄严体，历劫明心大法师。\n\n美猴王一见，倒身下拜，磕头不计其数，口中只道：“师父，师父！我弟子志心朝礼，志心朝礼！”祖师道：“你是那方人氏？且说个乡贯姓名明白，再拜。”猴王道：“弟子乃东胜神洲傲来国花果山水帘洞人氏。”祖师喝令：“赶出去！他本是个撒诈捣虚之徒，那里修什么道果！”猴王慌忙磕头不住道：“弟子是老实之言，决无虚诈。”祖师道：“你既老实，怎么说东胜神洲？那去处到我这里，隔两重大海，一座南赡部洲，如何就得到此？”猴王叩头道：“弟子飘洋过海，登界游方，有十数个年头，方才访到此处。”祖师道：“既是逐渐行来的也罢。你姓什么？”猴王又道：“我无性。人若骂我我也不恼，若打我我也不嗔，只是陪个礼儿就罢了，一生无性。”祖师道：“不是这个性。你父母原来姓什么？”猴王道：“我也无父母。”祖师道：“既无父母，想是树上生的？”猴王道：“我虽不是树上生，却是石里长的。我只记得花果山上有一块仙石，其年石破，我便生也。”祖师闻言暗喜道：“这等说，却是个天地生成的，你起来走走我看。”猴王纵身跳起，拐呀拐的走了两遍。\n\n祖师笑道：“你身躯虽是鄙陋，却像个食松果的猢狲。我与你就身上取个姓氏，意思教你姓‘猢’。猢字去了个兽旁，乃是个古月。古者老也，月者阴也。老阴不能化育，教你姓‘狲’倒好。狲字去了兽旁，乃是个子系。子者儿男也，系者婴细也，正合婴儿之本论，教你姓‘孙’罢。”猴王听说，满心欢喜，朝上叩头道：“好，好，好！今日方知姓也。万望师父慈悲，既然有姓，再乞赐个名字，却好呼唤。”祖师道：“我门中有十二个字，分派起名，到你乃第十辈之小徒矣。”猴王道：“那十二个字？”祖师道：“乃‘广大智慧真如性海颖悟圆觉’十二字。排到你，正当‘悟’字。与你起个法名叫做‘孙悟空’，好么？”猴王笑道：“好，好，好！自今就叫做孙悟空也！”正是：鸿蒙初辟原无姓，打破顽空须悟空。毕竟不知向后修些什么道果，且听下回分解。	', 0, '2022-10-24 21:16:09', '2022-10-24 21:16:09', 'd65e3b18dd8789c80ccadf238b812529', 0);
INSERT INTO `chapter` VALUES ('d4eb740ddb963b49005c36b06b71bb34', '观音院唐僧脱难　高老庄行者降魔', NULL, 0, '2022-10-31 08:11:38', '2022-10-31 08:11:38', 'd65e3b18dd8789c80ccadf238b812529', 0);
INSERT INTO `chapter` VALUES ('d960c220afc63b19d3f33cbdb92ba287', '邪魔侵正法　意马忆心猿', NULL, 0, '2022-10-31 08:12:42', '2022-10-31 08:12:42', 'd65e3b18dd8789c80ccadf238b812529', 0);
INSERT INTO `chapter` VALUES ('db82ea87c9cd4afeb8fa236143a5ee74', '孙行者大闹黑风山　观世音收伏熊罴怪', NULL, 0, '2022-10-31 08:11:33', '2022-10-31 08:11:33', 'd65e3b18dd8789c80ccadf238b812529', 0);
INSERT INTO `chapter` VALUES ('e80614ec93cfa6349fc7cbb749acac1a', '黄风岭唐僧有难　半山中八戒争先', NULL, 0, '2022-10-31 08:11:48', '2022-10-31 08:11:48', 'd65e3b18dd8789c80ccadf238b812529', 0);

-- ----------------------------
-- Table structure for collection
-- ----------------------------
DROP TABLE IF EXISTS `collection`;
CREATE TABLE `collection`  (
  `collection_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '收藏ID',
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户ID',
  `novel_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '小说ID',
  `is_deleted` int(1) NULL DEFAULT 0 COMMENT '逻辑删除',
  `create_time` timestamp(0) NULL DEFAULT NULL COMMENT '字段创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '字段修改时间',
  `is_collection` int(1) UNSIGNED ZEROFILL NULL DEFAULT 0 COMMENT '是否收藏，0为未收藏，1为收藏\r\n',
  PRIMARY KEY (`collection_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of collection
-- ----------------------------
INSERT INTO `collection` VALUES ('336100f9396d46dd7c96bb0831c41713', 'fd51029adad0a27874448b1afd6716fc', 'd65e3b18dd8789c80ccadf238b812529', 0, '2022-10-31 10:27:48', '2022-10-31 10:27:48', 1);
INSERT INTO `collection` VALUES ('69aac5e060b930d32f38751ec6db28ea', 'fd51029adad0a27874448b1afd6716fc', 'd65e3b18dd8789c80ccadf238b812529', 1, '2022-10-31 10:08:18', '2022-10-31 10:13:23', 1);
INSERT INTO `collection` VALUES ('aa6240d4ac452f671e00b3b0b8c9e19a', 'fd51029adad0a27874448b1afd6716fc', 'd65e3b18dd8789c80ccadf238b812529', 1, '2022-10-31 10:20:35', '2022-10-31 10:27:48', 1);
INSERT INTO `collection` VALUES ('b6a73fd73166b2aa714c17daeaf2a7fd', 'fd51029adad0a27874448b1afd6716fc', '87ac3f6c12e04cf921a7cac9bf0ed299', 0, '2022-10-31 10:07:57', '2022-10-31 10:07:57', 1);

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment`  (
  `comment_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '评论ID',
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户ID',
  `novel_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '小说ID',
  `comment_content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '评价内容(具体文字)',
  `is_deleted` int(1) NULL DEFAULT 0 COMMENT '逻辑删除',
  `create_time` timestamp(0) NULL DEFAULT NULL COMMENT '字段创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '字段修改时间',
  PRIMARY KEY (`comment_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of comment
-- ----------------------------
INSERT INTO `comment` VALUES ('0cf60b94e51bc4e7a271d34b1066e1a7', 'fd51029adad0a27874448b1afd6716fc', 'd65e3b18dd8789c80ccadf238b812529', '好喜欢悟静的大胡子啊！', 0, '2022-10-28 17:19:07', '2022-10-28 17:19:07');
INSERT INTO `comment` VALUES ('2438156deb0ad78c2fa9f02d26daf95d', 'fd51029adad0a27874448b1afd6716fc', 'd65e3b18dd8789c80ccadf238b812529', '真的服了', 0, '2022-10-30 20:37:51', '2022-10-30 20:37:51');
INSERT INTO `comment` VALUES ('2b50f2a7687698e4949e9f9b0813f670', 'fd51029adad0a27874448b1afd6716fc', '3fa6e1d6b7e9b31ebdbd01b778df5e3b', '好恐怖啊，好温情啊！！', 0, '2022-10-28 16:14:23', '2022-10-28 16:14:23');
INSERT INTO `comment` VALUES ('2c500ef4a7cf619614976f983cb88916', 'fd51029adad0a27874448b1afd6716fc', '87ac3f6c12e04cf921a7cac9bf0ed299', '乔峰好帅啊！', 0, '2022-10-28 16:15:50', '2022-10-28 16:15:50');
INSERT INTO `comment` VALUES ('2e7c85e7c67c33bfede1807a0b1a50b3', 'fd51029adad0a27874448b1afd6716fc', '7a59814935fee4096f30ed29567010b0', '我好像宋江的格局啊', 0, '2022-10-30 20:41:10', '2022-10-30 20:41:10');
INSERT INTO `comment` VALUES ('343081853a76c144693d673be038edd2', 'fd51029adad0a27874448b1afd6716fc', 'd65e3b18dd8789c80ccadf238b812529', '', 0, '2022-10-30 20:38:55', '2022-10-30 20:38:55');
INSERT INTO `comment` VALUES ('35a0babbbf76a411380420ffd25ad004', 'fd51029adad0a27874448b1afd6716fc', 'd65e3b18dd8789c80ccadf238b812529', '6666666666', 0, '2022-10-31 09:42:59', '2022-10-31 09:42:59');
INSERT INTO `comment` VALUES ('392790968b2b8fb5c738732530cbabd3', 'fd51029adad0a27874448b1afd6716fc', '87bd7a82684fc08eeb3c734d8c92e11d', '诸葛先生好聪明啊！！！', 0, '2022-10-28 16:11:29', '2022-10-28 16:11:29');
INSERT INTO `comment` VALUES ('43a033455509facc3eed9f8b8781ee17', '1f8339b769aad11fa78f5f2493b11c8c', '87bd7a82684fc08eeb3c734d8c92e11d', '我要丈八蛇矛', 0, '2022-10-28 21:23:06', '2022-10-28 21:23:06');
INSERT INTO `comment` VALUES ('48cbf61ef115cb683cf4a02e4619eaa9', 'fd51029adad0a27874448b1afd6716fc', '7a59814935fee4096f30ed29567010b0', '好喜欢豹子头林冲啊', 0, '2022-10-30 20:40:58', '2022-10-30 20:40:58');
INSERT INTO `comment` VALUES ('4b391e1c754976c065cae9fcc433a723', '1f8339b769aad11fa78f5f2493b11c8c', '87bd7a82684fc08eeb3c734d8c92e11d', '刘备仁义 啊', 0, '2022-10-28 21:22:31', '2022-10-28 21:22:31');
INSERT INTO `comment` VALUES ('4dafe3cb8d9ef2f21d0dda1cd261a219', '1f8339b769aad11fa78f5f2493b11c8c', '87bd7a82684fc08eeb3c734d8c92e11d', '1', 1, '2022-10-28 21:23:13', '2022-10-30 09:25:36');
INSERT INTO `comment` VALUES ('6758428bfd7ce9ae42a5152842225918', '420b28d77a716189698e7c45bf9d7d2a', '87bd7a82684fc08eeb3c734d8c92e11d', '我好喜欢林妹妹啊！！', 0, '2022-10-28 16:10:28', '2022-10-28 16:10:28');
INSERT INTO `comment` VALUES ('80a2d96cc7cc928d31d744688dafe1ae', '1f8339b769aad11fa78f5f2493b11c8c', '87bd7a82684fc08eeb3c734d8c92e11d', '我想要一把方天画戟欸', 0, '2022-10-28 21:22:54', '2022-10-28 21:22:54');
INSERT INTO `comment` VALUES ('831d823b99f8d2d782baec92d6c7dd84', 'fd51029adad0a27874448b1afd6716fc', 'd65e3b18dd8789c80ccadf238b812529', '大鹏好帅啊！', 0, '2022-10-30 20:37:04', '2022-10-30 20:37:04');
INSERT INTO `comment` VALUES ('83546eb72dcb2d9f58e42597afe1cad4', 'fd51029adad0a27874448b1afd6716fc', '7a59814935fee4096f30ed29567010b0', '我看了水浒传！', 0, '2022-10-28 16:19:22', '2022-10-28 16:19:22');
INSERT INTO `comment` VALUES ('856b1100cb538474130bee47a225052a', '1f8339b769aad11fa78f5f2493b11c8c', '87bd7a82684fc08eeb3c734d8c92e11d', '子龙好强啊！', 0, '2022-10-28 21:21:42', '2022-10-28 21:21:42');
INSERT INTO `comment` VALUES ('8a91ee7729f526e70df3035e2408ce61', 'fd51029adad0a27874448b1afd6716fc', 'd65e3b18dd8789c80ccadf238b812529', '4564545454', 0, '2022-10-31 10:20:48', '2022-10-31 10:20:48');
INSERT INTO `comment` VALUES ('8cafbec68f879f8a0413287557d97877', 'fd51029adad0a27874448b1afd6716fc', '4ab239075bfeb83b7e768bf43955d0b5', '我看了三体，好看', 0, '2022-10-28 16:19:54', '2022-10-28 16:19:54');
INSERT INTO `comment` VALUES ('968ac5070de1a6a6107946c77d3bc2a0', 'fd51029adad0a27874448b1afd6716fc', 'd65e3b18dd8789c80ccadf238b812529', '111', 0, '2022-10-30 20:39:01', '2022-10-30 20:39:01');
INSERT INTO `comment` VALUES ('9cb656babf77bff06b0e8e42a9b479b5', 'fd51029adad0a27874448b1afd6716fc', 'd65e3b18dd8789c80ccadf238b812529', '唐僧太装了，女儿国国王真是个恋爱脑啊！', 0, '2022-10-30 20:37:38', '2022-10-30 20:37:38');
INSERT INTO `comment` VALUES ('a1bf79ef9e85383ff49f2f8b9ca504a4', 'fd51029adad0a27874448b1afd6716fc', '10e624e4da4db30d308b2e973c0e5860', '我好喜欢蝴蝶啊', 0, '2022-10-28 17:17:27', '2022-10-28 17:17:27');
INSERT INTO `comment` VALUES ('b35d919fb363a51e0f4adf6e75f0b84a', 'fd51029adad0a27874448b1afd6716fc', 'd65e3b18dd8789c80ccadf238b812529', '好喜欢八戒哥哥啊', 0, '2022-10-30 20:36:30', '2022-10-30 20:36:30');
INSERT INTO `comment` VALUES ('daecda019ab3a170c1e5b154340656f0', 'fd51029adad0a27874448b1afd6716fc', '6a41229c9041193594ef590dbd0b2b3c', '好细腻的爱情啊', 0, '2022-10-28 17:23:56', '2022-10-28 17:23:56');
INSERT INTO `comment` VALUES ('e2ed03f2b0d4cf91a6b79090bad78e9b', '1f8339b769aad11fa78f5f2493b11c8c', '87bd7a82684fc08eeb3c734d8c92e11d', '2', 1, '2022-10-28 21:23:18', '2022-10-30 09:25:25');
INSERT INTO `comment` VALUES ('f08c00cd43bf01e9a504a40bc8b12f89', '420b28d77a716189698e7c45bf9d7d2a', '87bd7a82684fc08eeb3c734d8c92e11d', '我好喜欢湘云啊！！', 0, '2022-10-28 16:10:09', '2022-10-28 16:10:09');

-- ----------------------------
-- Table structure for novel
-- ----------------------------
DROP TABLE IF EXISTS `novel`;
CREATE TABLE `novel`  (
  `novel_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '小说ID',
  `novel_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '小说名称',
  `status` int(11) NULL DEFAULT 0 COMMENT '小说审核状态(0为审核未通过，1为审核通过)',
  `novel_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '小说简介',
  `is_deleted` int(1) NULL DEFAULT 0 COMMENT '逻辑删除(0为未删除，1为删除)',
  `create_time` timestamp(0) NULL DEFAULT NULL COMMENT '字段创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改字段时间',
  `type_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类ID',
  `novel_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '小说图片地址',
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '小说创作者(用户昵称)',
  `novel_hits` int(15) NULL DEFAULT 0 COMMENT '小说点击量',
  PRIMARY KEY (`novel_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of novel
-- ----------------------------
INSERT INTO `novel` VALUES ('10e624e4da4db30d308b2e973c0e5860', '梁山伯与祝英台', 1, '《梁山伯与祝英台》，简称《梁祝》，是中国古代民间四大爱情故事之一（另外三个为《白蛇传》、《孟姜女传说》和《牛郎织女》），是中国最具魅力的口头传承艺术及国家级非物质文化遗产，也是在世界上产生广泛影响的中国民间传说。自东晋始，梁祝故事在民间流传已有1700多年，在中国可谓家喻户晓，被誉为爱情的千古绝唱。', 0, '2022-10-27 15:12:41', '2022-10-30 16:28:10', '05fb1b9630f2b7f20eb31f5d84d51ae1', 'https://ts1.cn.mm.bing.net/th/id/R-C.7d3a52f53f9013c1e14365dd05c0b6bc?rik=92gUS9NFeGvxKQ&riu=http%3a%2f%2fpic01.tuku.com.cn%2ffile_thumb%2f201208%2fm2012082405192122.jpg&ehk=qxwAkOoTXtaumi9TjEC1%2b0PNQ4OaFHnlK%2fIJpdWIbqI%3d&risl=&pid=ImgRaw&r=0', '1f8339b769aad11fa78f5f2493b11c8c', 38);
INSERT INTO `novel` VALUES ('253cd018ff23fc77f7215ddce68a05d3', '神雕侠侣', 1, '小说的主脉写的是杨康之遗孤杨过与其师小龙女之间的爱情故事。杨过14岁起师从18岁的小龙女，于古墓派之中苦练武功，师徒二人情深义重、日久生情，却无奈于江湖阴鸷险恶、蒙古铁骑来犯使得有情之人难成眷属。历经一番坎坷与磨难的考验，杨过冲破封建礼教之禁锢，最终与小龙女由师徒变为“侠侣”。同时，在这段磨难经历中，杨过也消除了对郭靖、黄蓉夫妇的误会，在家仇与国难间作出抉择，成为真正的“侠之大者”。', 0, '2022-10-27 15:22:40', '2022-10-30 10:05:17', 'acc6564f62d278dcf7287a3b6b9f6d6f', 'https://ts1.cn.mm.bing.net/th/id/R-C.8d073796f4caa5d6aef997cd49d627f5?rik=QHYJk4eqNXGIwA&riu=http%3a%2f%2fn.sinaimg.cn%2fsinacn%2fw1600h1123%2f20180112%2f75ba-fyqnick9255240.jpg&ehk=hZxer9IhPK9F6Bq6Ay41T2wDQUhhqtAzchsADar6zGc%3d&risl=&pid=ImgRaw&r=0', '420b28d77a716189698e7c45bf9d7d2a', 101);
INSERT INTO `novel` VALUES ('3fa6e1d6b7e9b31ebdbd01b778df5e3b', '聊斋志异', 1, '《聊斋志异》（简称《聊斋》，俗名《鬼狐传》）是中国清朝小说家蒲松龄创作的文言短篇小说集，最早的抄本在清代康熙年间已有流传。', 0, '2022-10-27 15:03:50', '2022-10-27 15:08:05', 'de46c58d81eb42dd35c414ab79100db2', 'https://img.zcool.cn/community/01bc2e5a52e3bca8012180c5e78678.jpg@1280w_1l_2o_100sh.jpg', 'fd51029adad0a27874448b1afd6716fc', 9);
INSERT INTO `novel` VALUES ('4ab239075bfeb83b7e768bf43955d0b5', '三体', 1, '作品讲述了地球人类文明和三体文明的信息交流、生死搏杀及两个文明在宇宙中的兴衰历程。其第一部经过刘宇昆翻译后获得了第73届雨果奖最佳长篇小说奖。', 0, '2022-10-27 15:01:02', '2022-10-27 15:01:33', '9405adbcf44d36210592651ec8ccb509', 'https://images.pexels.com/photos/2810753/pexels-photo-2810753.jpeg?auto=compress&cs=tinysrgb&w=1600&lazy=load', 'fd51029adad0a27874448b1afd6716fc', 6);
INSERT INTO `novel` VALUES ('6a41229c9041193594ef590dbd0b2b3c', '挪威的森林', 1, '作品—开始就写“我\"抵达汉堡机场时，听到了初恋情人直子最喜欢的这首歌，不由回忆起18年前直子和“我”一夜情后不辞而别，同班女生绿子走进了“我”的生活。而后，才知道直子进了精神病院。“我们”又在那里度过了几天快乐时光，还认识了其他几个病友。直子自杀后，“我们”在她的葬礼上用吉他弹这首歌，表示对她的怀念和对社会的不满与抗议。这本书表达了青少年面对青春期的孤独困惑以及面对成长的无奈，以及年轻人在社会压力', 0, '2022-10-27 15:21:05', '2022-10-27 15:24:30', '05fb1b9630f2b7f20eb31f5d84d51ae1', 'https://yingshi-stream.2345cdn.net/dypcimg/tv/img/special/0/9/4aa66442dadd9daf52eb9dcd7c592aff.jpeg', '420b28d77a716189698e7c45bf9d7d2a', 18);
INSERT INTO `novel` VALUES ('758b6e996824e0e3c518820c934dc8ab', '见鬼十法', 1, '见到鬼的十种方法', 0, '2022-10-30 20:47:58', '2022-10-30 20:48:09', 'de46c58d81eb42dd35c414ab79100db2', 'https://img.zcool.cn/community/01fec3554894850000019ae9acab17.jpg@1280w_1l_2o_100sh.jpg', 'fd51029adad0a27874448b1afd6716fc', 2);
INSERT INTO `novel` VALUES ('79ba52ac4fcb59d5db0a4f1baa73b422', '红楼梦', 1, '《红楼梦》，别名《石头记》等，中国古代章回体长篇小说，中国古典四大名著之一。其通行本共120回，一般认为前80回是清代作家曹雪芹所著，\n后40回作者为无名氏，整理者为程伟元、高鹗。小说以贾、史、王、薛四大家族的兴衰为背景，以富贵公子贾宝玉为视角，以贾宝玉与林黛玉、薛宝钗的爱情婚姻悲剧为主线\n，描绘了一些闺阁佳人的人生百态，展现了真正的人性美和悲剧美，是一部从各个角度展现女性美以及中国古代社会百态的', 0, '2022-10-31 10:21:38', '2022-10-31 10:21:51', '30ea27ea0429591dddd073aea7aaad8d', 'https://img.tukuppt.com/ad_preview/00/12/22/5c995933b0651.jpg!/fw/980', 'fd51029adad0a27874448b1afd6716fc', 1);
INSERT INTO `novel` VALUES ('7a59814935fee4096f30ed29567010b0', '水浒传', 1, '全书通过描写梁山好汉反抗欺压、水泊梁山壮大和受宋朝招安，以及受招安后为宋朝征战，最终消亡的宏大故事，艺术地反映了中国历史上宋江起义从发生、发展直至失败的全过程，深刻揭示了起义的社会根源，满腔热情地歌颂了起义英雄的反抗斗争和他们的社会理想，也具体揭示了起义失败的内在历史原因。 [28] ', 0, '2022-10-27 15:07:07', '2022-10-27 15:08:05', '30ea27ea0429591dddd073aea7aaad8d', 'https://ts1.cn.mm.bing.net/th/id/R-C.085ec7287f430ed3f25ebc915490ce58?rik=crTT3S8ebQ5PKw&riu=http%3a%2f%2fpic.baike.soso.com%2fp%2f20140603%2f20140603142128-1887100531.jpg&ehk=31moHjsEX9EYQAkuH71AbD17SwrRYWws4VVIVlxuslw%3d&risl=&pid=ImgRaw&r=0', 'fd51029adad0a27874448b1afd6716fc', 38);
INSERT INTO `novel` VALUES ('87ac3f6c12e04cf921a7cac9bf0ed299', '天龙八部', 1, '这部小说以宋哲宗时代为背景，通过宋、辽、大理、西夏、吐蕃等王国之间的武林恩怨和民族矛盾，从哲学的高度对人生和社会进行审视和描写，展示了一幅波澜壮阔的生活画卷。其故事之离奇曲折、涉及人物之众多、历史背景之广泛、武侠战役之庞大、想象力之丰富当属“金书”之最。作品风格宏伟悲壮，是一部写尽人性、悲剧色彩浓厚的史诗巨著。', 0, '2022-10-27 15:24:14', '2022-10-30 20:45:31', 'acc6564f62d278dcf7287a3b6b9f6d6f', 'https://puui.qpic.cn/qqvideo_ori/0/i0538frlvbs_496_280/0', '420b28d77a716189698e7c45bf9d7d2a', 158);
INSERT INTO `novel` VALUES ('87bd7a82684fc08eeb3c734d8c92e11d', '三国演义', 1, '《三国演义》可大致分为黄巾起义、董卓之乱、群雄逐鹿、三国鼎立、三国归晋五大部分，描写了从东汉末年到西晋初年之间近百年的历史风云，以描写战争为主，诉说了东汉末年的群雄割据混战和魏、蜀、吴三国之间的政治和军事斗争，最终司马炎一统三国，建立晋朝的故事。反映了三国时代各类社会斗争与矛盾的转化，并概括了这一时代的历史巨变，塑造了一群叱咤风云的三国英雄人物。', 0, '2022-10-27 15:07:56', '2022-10-27 15:08:06', '30ea27ea0429591dddd073aea7aaad8d', 'https://img.zcool.cn/community/01b6ca5e352c40a80120a895145eb4.jpg@1280w_1l_2o_100sh.jpg', 'fd51029adad0a27874448b1afd6716fc', 22);
INSERT INTO `novel` VALUES ('928a316142f737658fe043f4a5e44f27', '简爱', 1, '《简·爱》中简·爱的人生追求有两个基本旋律：富有激情、幻想、反抗和坚持不懈的精神；对人间自由幸福的渴望和对更高精神境界的追求。这本小说的主题是通过孤女坎坷不平的人生经历，成功地塑造了一个不安于现状、不甘受辱、敢于抗争的女性形象，反映一个平凡心灵的坦诚倾诉的呼号和责难，一个小写的人成为一个大写的人的渴望。 [4] ', 0, '2022-10-27 15:14:38', '2022-10-27 15:15:03', '05fb1b9630f2b7f20eb31f5d84d51ae1', 'https://ts1.cn.mm.bing.net/th/id/R-C.7b7dc089d69d0d87de7da9d065076378?rik=YEKk2X1M9UuvdA&riu=http%3a%2f%2fimage11.m1905.cn%2fuploadfile%2f2014%2f0211%2f20140211093252296092.jpg&ehk=p6B3kxiFDBr15TCfGCS%2f1EpYCcC8Tolw2b8x2YPIqqM%3d&risl=&pid=ImgRaw&r=0', '1f8339b769aad11fa78f5f2493b11c8c', 2);
INSERT INTO `novel` VALUES ('b37ec2091d6848eb93a7b9c22929df0e', '射雕英雄传', 1, '南宋时期，惨遭灭门横祸的郭靖、杨康分别在江南七怪与全真教道士丘处机的教养下成人。18年后，郭靖奉师命南下。杨康却贪恋富贵，认贼作父。郭靖与黄蓉一见如故，彼此倾心，但因华筝之婚约在先，以及江南七怪的反对等多种因素，两人情感可谓一波三折。五位师父被害于桃花岛，郭靖愤而离开黄蓉。', 0, '2022-10-25 14:36:54', '2022-10-25 14:37:19', 'acc6564f62d278dcf7287a3b6b9f6d6f', 'https://ts1.cn.mm.bing.net/th/id/R-C.f417c9afe05c3df3523ae7939027ae64?rik=oOghFMevsNVWLQ&riu=http%3a%2f%2fimg3.donews.com%2fuploads%2fimg3%2fimg_pic_1496739861_0.jpg&ehk=9MtPj9oC5lODCR4rQZgENlIF4S8wBP%2bqbne%2f6CS61FA%3d&risl=&pid=ImgRaw&r=0', 'fd51029adad0a27874448b1afd6716fc', 14);
INSERT INTO `novel` VALUES ('d65e3b18dd8789c80ccadf238b812529', '西游记', 1, '该小说主要讲述了孙悟空出世跟随菩提祖师学艺及大闹天宫后，遇见了唐僧、猪八戒、沙僧和白龙马，西行取经，一路上历经艰险，降妖除魔，经历了九九八十一难，终于到达西天见到如来佛祖，最终五圣成真的故事。该小说以“玄奘取经”这一历史事件为蓝本，经作者的艺术加工，深刻地描绘出明代百姓的社会生活状况。', 0, '2022-10-24 21:14:12', '2022-10-25 17:24:34', '30ea27ea0429591dddd073aea7aaad8d', 'https://img.zcool.cn/community/01537f5c308f5ea80121df90fab5ac.jpg@1280w_1l_2o_100sh.jpg', 'fd51029adad0a27874448b1afd6716fc', 99);
INSERT INTO `novel` VALUES ('ec04c8d6578e6d58239836234cab294c', '傲慢与偏见', 1, '《傲慢与偏见》（Pride and Prejudice）是英国女小说家简·奥斯汀创作的长篇小说。\n小说描写了小乡绅班纳特五个待字闺中的千金，主角是二女儿伊丽莎白。她在舞会上认识了达西，但是耳闻他为人傲慢，一直对他心生排斥，经历一番周折，伊丽莎白解除了对达西的偏见，达西也放下傲慢，有情人终成眷属。 ', 0, '2022-10-27 14:56:29', '2022-10-27 14:56:44', '05fb1b9630f2b7f20eb31f5d84d51ae1', 'https://img1.baidu.com/it/u=4188834323,300107640&fm=253&fmt=auto&app=138&f=JPEG?w=669&h=450', 'fd51029adad0a27874448b1afd6716fc', 81);

-- ----------------------------
-- Table structure for novel_chapter
-- ----------------------------
DROP TABLE IF EXISTS `novel_chapter`;
CREATE TABLE `novel_chapter`  (
  `novel_chapter_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '小说章节ID',
  `novel_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '小说ID',
  `chapter` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '章节ID',
  `is_deleted` int(1) NULL DEFAULT 0 COMMENT '逻辑删除',
  `create_time` timestamp(0) NULL DEFAULT NULL COMMENT '字段创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '字段修改时间',
  PRIMARY KEY (`novel_chapter_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for type
-- ----------------------------
DROP TABLE IF EXISTS `type`;
CREATE TABLE `type`  (
  `type_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分类ID',
  `type_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类名称(如:武侠、玄幻、言情)',
  `is_deleted` int(1) NULL DEFAULT 0 COMMENT '逻辑删除',
  `create_time` timestamp(0) NULL DEFAULT NULL COMMENT '字段创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '字段修改时间',
  PRIMARY KEY (`type_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of type
-- ----------------------------
INSERT INTO `type` VALUES ('05fb1b9630f2b7f20eb31f5d84d51ae1', 'love', 0, '2022-10-24 21:12:15', '2022-10-30 20:50:08');
INSERT INTO `type` VALUES ('2214053ef8bb68abab923907562aaa6e', '诗词', 0, '2022-10-31 10:24:06', '2022-10-31 10:24:06');
INSERT INTO `type` VALUES ('30ea27ea0429591dddd073aea7aaad8d', '文学', 0, '2022-10-24 21:12:05', '2022-10-24 21:12:05');
INSERT INTO `type` VALUES ('9405adbcf44d36210592651ec8ccb509', '玄幻', 0, '2022-10-24 21:12:25', '2022-10-24 21:12:25');
INSERT INTO `type` VALUES ('acc6564f62d278dcf7287a3b6b9f6d6f', '武侠', 0, '2022-10-25 14:27:03', '2022-10-25 14:27:03');
INSERT INTO `type` VALUES ('de46c58d81eb42dd35c414ab79100db2', '惊悚', 0, '2022-10-24 21:12:20', '2022-10-24 21:12:20');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户ID',
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名（账号）',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `wallet_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '钱包ID',
  `identity` int(1) NULL DEFAULT 0 COMMENT '身份(0为普通读者，1为待审核(就是点击申请成为作者但还没通过的状态))，2为作者',
  `is_deleted` int(1) NULL DEFAULT 0 COMMENT '逻辑删除',
  `create_time` timestamp(0) NULL DEFAULT NULL COMMENT '字段创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '字段修改时间',
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号码',
  `nick_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '昵称(网名)',
  `head_portrait` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像地址',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1f8339b769aad11fa78f5f2493b11c8c', '2', '2', '9b66915164c062d1e84363e3f1ccc5e5', 2, 0, '2022-10-27 15:11:00', '2022-10-27 15:11:56', '1888888', '张旭', NULL);
INSERT INTO `user` VALUES ('420b28d77a716189698e7c45bf9d7d2a', '3', '3', '1cb510b9689bdfc6dbd8540285297eae', 2, 0, '2022-10-27 15:16:20', '2022-10-27 15:17:29', '1333333333', '张桑', NULL);
INSERT INTO `user` VALUES ('fd51029adad0a27874448b1afd6716fc', '1', '1', 'e438102b9590447f9e3cd8c7764d8048', 2, 0, '2022-10-24 20:43:50', '2022-10-31 10:21:02', '18507413701', '张峰华', NULL);

-- ----------------------------
-- Table structure for wallet
-- ----------------------------
DROP TABLE IF EXISTS `wallet`;
CREATE TABLE `wallet`  (
  `wallet_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '钱包ID',
  `balance` int(10) NULL DEFAULT NULL COMMENT '余额',
  `frozen_balance` int(10) NULL DEFAULT NULL COMMENT '冻结余额',
  `is_deleted` int(1) NULL DEFAULT 0 COMMENT '逻辑删除',
  `create_time` timestamp(0) NULL DEFAULT NULL COMMENT '字段创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '字段修改时间',
  PRIMARY KEY (`wallet_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wallet
-- ----------------------------
INSERT INTO `wallet` VALUES ('1cb510b9689bdfc6dbd8540285297eae', 144, 0, 0, '2022-10-27 15:16:21', '2022-10-30 22:05:05');
INSERT INTO `wallet` VALUES ('9b66915164c062d1e84363e3f1ccc5e5', 45, 0, 0, '2022-10-27 15:11:00', '2022-10-30 22:04:32');
INSERT INTO `wallet` VALUES ('e438102b9590447f9e3cd8c7764d8048', 5515, 0, 0, '2022-10-24 20:43:51', '2022-10-31 09:09:39');

-- ----------------------------
-- Table structure for withdrawal
-- ----------------------------
DROP TABLE IF EXISTS `withdrawal`;
CREATE TABLE `withdrawal`  (
  `withdrawal_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '提现审核ID',
  `wallet_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '钱包ID',
  `withdrawal_money` int(20) NULL DEFAULT 0 COMMENT '提现金额',
  `status` int(1) NULL DEFAULT 0 COMMENT '提现审核状态(0代表未通过，1审核中，2代表审核通过)',
  `is_deleted` int(1) NULL DEFAULT 0 COMMENT '逻辑删除',
  `create_time` timestamp(0) NULL DEFAULT NULL COMMENT '字段创建时间',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '字段修改时间',
  PRIMARY KEY (`withdrawal_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of withdrawal
-- ----------------------------
INSERT INTO `withdrawal` VALUES ('093a66a28b4b2bf5166bb222c22157c9', 'e438102b9590447f9e3cd8c7764d8048', 10, 2, 0, '2022-10-28 14:13:43', '2022-10-28 14:14:35');
INSERT INTO `withdrawal` VALUES ('1dbbe2f4ee8b6098a026518d5ecd4bfc', 'e438102b9590447f9e3cd8c7764d8048', 5, 2, 0, '2022-10-30 16:34:53', '2022-10-30 16:34:59');
INSERT INTO `withdrawal` VALUES ('221ad59b5efdc1e29425c6d02173ca2e', 'e438102b9590447f9e3cd8c7764d8048', 5, 2, 0, '2022-10-30 20:45:57', '2022-10-30 20:46:05');
INSERT INTO `withdrawal` VALUES ('2ad51a6d52ff13098efc4e02dfe3bc03', 'e438102b9590447f9e3cd8c7764d8048', 0, 2, 0, '2022-10-28 14:16:17', '2022-10-28 14:19:07');
INSERT INTO `withdrawal` VALUES ('2ccf3cd62d33f565c0a6446674859eaa', 'e438102b9590447f9e3cd8c7764d8048', 5, 2, 0, '2022-10-28 14:20:06', '2022-10-29 20:39:06');
INSERT INTO `withdrawal` VALUES ('4c04d7d560a0b67fab32ba83546183a0', 'e438102b9590447f9e3cd8c7764d8048', 5, 2, 0, '2022-10-28 14:19:24', '2022-10-28 14:19:50');
INSERT INTO `withdrawal` VALUES ('530bae447f4716be07af494ccb5fa88c', 'e438102b9590447f9e3cd8c7764d8048', 30, 2, 0, '2022-10-31 09:09:26', '2022-10-31 09:09:33');
INSERT INTO `withdrawal` VALUES ('6ce2d12dbdbaed9c58e0f5c4be596179', 'e438102b9590447f9e3cd8c7764d8048', 5, 2, 0, '2022-10-28 14:14:58', '2022-10-28 14:15:47');
INSERT INTO `withdrawal` VALUES ('8850c988e72bd4c629f7cd623d312b99', 'e438102b9590447f9e3cd8c7764d8048', 5, 2, 0, '2022-10-28 14:13:34', '2022-10-28 14:14:35');
INSERT INTO `withdrawal` VALUES ('a9d5ddacf60f291767605e6061ea399d', 'e438102b9590447f9e3cd8c7764d8048', 5, 2, 0, '2022-10-28 14:16:13', '2022-10-28 14:19:08');
INSERT INTO `withdrawal` VALUES ('dba268d1afbb99be2efc65dfed9d5791', 'e438102b9590447f9e3cd8c7764d8048', 10, 2, 0, '2022-10-29 20:39:19', '2022-10-29 20:39:34');

SET FOREIGN_KEY_CHECKS = 1;

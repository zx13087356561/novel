package com.hua.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hua.handler.Result;
import com.hua.pojo.Advert;
import com.hua.service.AdvertService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;


@RestController
@RequestMapping("/advert")
public class AdvertController {

    @Resource
    private AdvertService advertService;

    @PostMapping
    //post为新增
    public Result<?> advertsave(@RequestBody Advert advert){
        advertService.save(advert);
        return Result.success();
    }

    @PutMapping
    //put为更新
    public Result<?> advertUpdate(@RequestBody Advert advert){
        advertService.updateById(advert);
        return Result.success();
    }


    @GetMapping
    //Get为查询
    public Result<?> advertFindPage(@RequestParam(defaultValue = "1") Integer pageNum,
                              @RequestParam(defaultValue = "10") Integer pageSize,
                              @RequestParam(defaultValue = "") String search){
        LambdaQueryWrapper<Advert> wrapper= Wrappers.<Advert>lambdaQuery();
        if (StrUtil.isNotBlank(search)){
            wrapper.like(Advert::getStatus,search);
        }
        Page<Advert> page = advertService.page(new Page<>(pageNum, pageSize), wrapper);
        return Result.success(page);
    }

    @DeleteMapping
    //Delete为删除，removeByIds为批量删除
    public Result<?> advertDelete(@RequestBody Set<String> ids){
        advertService.removeByIds(ids);
        return Result.success();
    }


    //查询数据库广告表中的所有数据
    @PostMapping("/selectAdvert")
    public Result<?> selectAdvert(){
        List<Advert> list = advertService.list();
        return Result.success(list);
    }
}

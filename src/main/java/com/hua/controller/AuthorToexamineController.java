package com.hua.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hua.handler.Result;
import com.hua.pojo.AuthorToexamine;
import com.hua.pojo.User;
import com.hua.pojo.Withdrawal;
import com.hua.service.AuthorToexamineService;
import com.hua.service.UserService;
import com.hua.service.WithdrawalService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Set;
//作家审核
@RestController
@RequestMapping("/authorToexamine")
public class AuthorToexamineController {

    @Resource
    private UserService userService;


    @GetMapping
    //Get为查询
    public Result<?> authorToexamineFindPage(@RequestParam(defaultValue = "1") Integer pageNum,
                                             @RequestParam(defaultValue = "10") Integer pageSize,
                                             @RequestParam(defaultValue = "") String search){
        LambdaQueryWrapper<User> wrapper= Wrappers.<User>lambdaQuery();
        if (StrUtil.isNotBlank(search)){
            wrapper.like(User::getNickName,search);
        }
        Page<User> page = userService.page(new Page<>(pageNum, pageSize), wrapper);
        return Result.success(page);
    }

    @PutMapping
    //put为更新
    public Result<?> authorToexamineUpdate(@RequestBody User user ){
        userService.updateById(user);
        return Result.success();
    }


}

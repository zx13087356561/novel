package com.hua.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.hua.handler.Result;
import com.hua.pojo.Chapter;
import com.hua.pojo.Novel;
import com.hua.service.ChapterService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/chapter")
public class ChapterController {
    @Resource
    private ChapterService chapterService;

    //新增一条章节数据
    @PostMapping("/addChapter")
    public Result<?> chapterSave(@RequestBody Chapter chapter){
        chapterService.save(chapter);
        return Result.success();
    }

    //根据小说id查询章节名称,这本小说有多少章节
    @PostMapping("/selectChapter/{novelId}")
    public Result<?> selectChapter(@PathVariable String novelId){
        LambdaQueryWrapper<Chapter> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Chapter::getNovelId,novelId);
        wrapper.orderByAsc(Chapter::getCreateTime);
        List<Chapter> list = chapterService.list(wrapper);
        int index = 1;
        for(Chapter chapter:list){
            chapter.setChapterIndex(index);
            index++;
        }
        return Result.success(list);
    }

    //根据章节ID(chapterId)更新章节内容
    @PutMapping()
    public Result<?> selectChapterContent(@RequestBody Chapter chapter){
        LambdaQueryWrapper<Chapter> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Chapter::getChapterId,chapter.getChapterId());
        Chapter one = chapterService.getOne(wrapper);
        one.setChapterContent(chapter.getChapterContent());
        chapterService.updateById(one);
        return Result.success();
    }

    //根据章节ID(chapterId)更新章节名称
    @PutMapping("/updataChapterName")
    public Result<?> updataChapterName(@RequestBody Chapter chapter){
        LambdaQueryWrapper<Chapter> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Chapter::getChapterId,chapter.getChapterId());
        Chapter one = chapterService.getOne(wrapper);
        one.setChapterName(chapter.getChapterName());
        chapterService.updateById(one);
        return Result.success();
    }

    //根据章节id查询章节内容
    @PostMapping("/selectChapterContent/{chapterId}")
    public Result<?> selectChapterContent(@PathVariable String chapterId){
        LambdaQueryWrapper<Chapter> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Chapter::getChapterId,chapterId);
        Chapter one = chapterService.getOne(wrapper);
        String content=one.getChapterContent();
        return Result.success(content);
    }

    //根据章节id查询这一行的数据
    @PostMapping("/selectChapterAll/{chapterId}")
    public Result<?> selectChapterAll(@PathVariable String chapterId){
        Chapter byId = chapterService.getById(chapterId);
        return Result.success(byId);
    }

    //根据章节id删除章节
    @DeleteMapping("/deleteChapter/{chapterId}")
    public Result<?> deleteChapter(@PathVariable String chapterId){
        chapterService.removeById(chapterId);
        return Result.success();
    }

}

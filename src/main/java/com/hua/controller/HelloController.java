package com.hua.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Controller + @ResponseBody=@RestController
 * @Controller；在一个类上添加@Controller注解，表明了这个类是一个控制器类
 */
@RestController
public class HelloController {
    @RequestMapping("/hello")
    public String Hello(){
        return "Hello huazi!";
    }
}

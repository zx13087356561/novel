package com.hua.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hua.handler.Result;
import com.hua.pojo.Novel;
import com.hua.service.NovelService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Set;

@RestController
@RequestMapping("/novelToexamine")
public class NovelToexamineController {

    @Resource
    private NovelService novelToexamineService;


    @GetMapping
    //Get为查询
    public Result<?> novelToexamineFindPage(@RequestParam(defaultValue = "1") Integer pageNum,
                                  @RequestParam(defaultValue = "10") Integer pageSize,
                                  @RequestParam(defaultValue = "") String search){
        LambdaQueryWrapper<Novel> wrapper= Wrappers.<Novel>lambdaQuery();
        if (StrUtil.isNotBlank(search)){
            wrapper.like(Novel::getNovelId,search);
        }
        Page<Novel> page = novelToexamineService.page(new Page<>(pageNum, pageSize), wrapper);
        return Result.success(page);
    }

    @PutMapping
    //put为更新
    public Result<?> novelToexamineUpdate(@RequestBody Novel novelToexamine ){
        novelToexamineService.updateById(novelToexamine);
        return Result.success();
    }


    @DeleteMapping
    //Delete为删除，removeByIds为批量删除
    public Result<?>WithdrawalDelete(@RequestBody Set<String> ids){
        novelToexamineService.removeByIds(ids);
        return Result.success();
    }
}

package com.hua.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.hua.handler.Result;
import com.hua.pojo.User;
import com.hua.service.UserService;
import org.apache.ibatis.annotations.Param;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
/**
 * @RestController=Controller+ResponseBoby
 * 　在一个类上添加@Controller注解，表明了这个类是一个控制器类
 * @ResponseBody表示方法的返回值直接以指定的格式写入Http response body中，而不是解析为跳转路径。
 * @RequestMapping注解是用来映射请求的，即指明处理器可以处理哪些URL请求，该注解既可以用在类上，也可以用在方法上。
 */
@RestController
@RequestMapping("/register")
public class Register {

    @Resource
    private UserService userService;

    @PostMapping
    //post为新增
    public Result<?> register(@RequestBody User user){
        //拿一个User类型的查询条件 where
        LambdaQueryWrapper<User> usersLambdaQueryWrapper = new LambdaQueryWrapper<>();
        //如何查询出数据库中没有该usernam则成功注册，否则用户名重复
        User serviceOne = userService.getOne(usersLambdaQueryWrapper.eq(User::getUserName,user.getUserName()));
        if (serviceOne == null){
            userService.save(user);
            return Result.success(user.getUserId());
        }
        return Result.error("401","用户名存在请重新设置！");
    }
}

package com.hua.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.hua.handler.Result;
import com.hua.pojo.User;
import com.hua.service.UserService;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/signIn")
public class SignIn {
    @Resource
    private UserService userService;
    //登录
    @PostMapping
    public Result<?> register(@RequestBody User user){
        //拿一个User类型的查询条件 where
        LambdaQueryWrapper<User> usersLambdaQueryWrapper = new LambdaQueryWrapper<>();
        //给查询条件 ... userName= "sa"  and  =
        User serviceOne = userService.getOne(usersLambdaQueryWrapper.eq(User::getUserName,user.getUserName()).eq(User::getPassword,user.getPassword()));
        if (serviceOne == null){
            return Result.error("401","账号密码错误！");
        }
        return Result.success(serviceOne);
    }

    //查询浏览器缓存userid是否相等数据库userid从而来更新个人中心里的信息
    @PostMapping("/personalCenter/{userId}")
    public Result<?>  personalCenter(@PathVariable String userId){
        //拿一个User类型的查询条件 where
        LambdaQueryWrapper<User> usersLambdaQueryWrapper = new LambdaQueryWrapper<>();
        User user1=userService.getOne(usersLambdaQueryWrapper.eq(User::getUserId,userId));
        return Result.success(user1);
    }


}

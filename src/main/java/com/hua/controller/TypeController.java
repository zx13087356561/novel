package com.hua.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hua.handler.Result;
import com.hua.pojo.Collection;
import com.hua.pojo.Type;
import com.hua.pojo.User;
import com.hua.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@RestController
@RequestMapping("/type")
public class TypeController {

    @Resource
    private TypeService typeService;

    @PostMapping
    //post为新增
    public Result<?> typesave(@RequestBody Type type){
        typeService.save(type);
        return Result.success();
    }

    @PutMapping
    //put为更新
    public Result<?> typeUpdate(@RequestBody Type type){
        typeService.updateById(type);
        return Result.success();
    }


    @GetMapping
    //Get为查询
    public Result<?> typeFindPage(@RequestParam(defaultValue = "1") Integer pageNum,
                              @RequestParam(defaultValue = "10") Integer pageSize,
                              @RequestParam(defaultValue = "") String search){
        LambdaQueryWrapper<Type> wrapper= Wrappers.<Type>lambdaQuery();
        if (StrUtil.isNotBlank(search)){
            wrapper.like(Type::getTypeName,search);
        }
        //mabatis-plus自带的分页查询
        Page<Type> page = typeService.page(new Page<>(pageNum, pageSize), wrapper);
        return Result.success(page);
    }

    @DeleteMapping
    //Delete为删除，removeByIds为批量删除
    public Result<?> typeDelete(@RequestBody Set<String> ids){
        typeService.removeByIds(ids);
        return Result.success();
    }
    //查询分类表中所有数据
    @PostMapping("/selectType")
    public Result<?> selectType(){
        List<Type> list = typeService.list();
        return Result.success(list);
    }
}

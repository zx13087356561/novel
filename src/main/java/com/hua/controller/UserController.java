package com.hua.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hua.handler.Result;
import com.hua.pojo.User;
import com.hua.service.UserService;
import com.hua.service.WalletService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;


@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserService userService;
    @Resource
    private WalletService walletService;

    @PostMapping
    //post为新增
    public Result<?> usersave(@RequestBody User user){
        //拿一个User类型的查询条件 where
        LambdaQueryWrapper<User> usersLambdaQueryWrapper = new LambdaQueryWrapper<>();
        //如何查询出数据库中没有该usernam则成功注册，否则用户名重复
        User serviceOne = userService.getOne(usersLambdaQueryWrapper.eq(User::getUserName,user.getUserName()));
        if (serviceOne == null){
            userService.save(user);
            return Result.success(user.getUserId());
        }
        return Result.error("401","用户名存在请重新设置！");
    }

    @PutMapping
    //put为更新
    public Result<?> userUpdate(@RequestBody User user){
        userService.updateById(user);
        return Result.success(user);
    }


    @GetMapping
    //Get为查询
    public Result<?> userFindPage(@RequestParam(defaultValue = "1") Integer pageNum,
                              @RequestParam(defaultValue = "10") Integer pageSize,
                              @RequestParam(defaultValue = "") String search){
        LambdaQueryWrapper<User> wrapper= Wrappers.<User>lambdaQuery();
        if (StrUtil.isNotBlank(search)){
            wrapper.like(User::getNickName,search);
        }
        Page<User> page = userService.page(new Page<>(pageNum, pageSize), wrapper);
        //您可以使用 getRecords 方法来获取 API 请求中指定的所有用户数据。records里面保存了User表中的所有数据
        List<User> records = page.getRecords();
        //循环查询
        records.forEach(item -> {
            /**
             *             selectBanlance的作用是查询出wallet表中的walletId和user表中的的walletId,
             *             传入（user表中的walletId）item.getWalletId()
             *             Long along返回的是wallet表中的余额值
             */
            Long aLong = walletService.selectBalance(item.getWalletId());
            //由于要使用balance（余额）来替换之前表格中的walletId所以要对aLong进行数据类型转换
            item.setWalletId(String.valueOf(aLong));
        });
        return Result.success(page);
    }

    @DeleteMapping
    //Delete为删除，removeByIds为批量删除
    public Result<?> userDelete(@RequestBody Set<String> ids){
        userService.removeByIds(ids);
        return Result.success();
    }
    //根据id查询单行数据
    @PostMapping("/selectUser/{userId}")
    public Result<?> selectUser(@PathVariable String userId){
        User byId = userService.getById(userId);
        return Result.success(byId);
    }
    //查询用户表中所有数据
    @GetMapping("/selectUser")
    public Result<?> selectUser(){
        List<User> list = userService.list();
        return Result.success(list);
    }
}

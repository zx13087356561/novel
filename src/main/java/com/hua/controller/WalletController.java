package com.hua.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.hua.handler.Result;
import com.hua.pojo.Wallet;
import com.hua.service.WalletService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/wallet")
public class WalletController {

    @Resource
    private WalletService walletService;
    /**
     * 新增一个钱包数据
     */
    @PostMapping
    public Result<?> addWallet(@RequestBody Wallet wallet){
        walletService.save(wallet);
        System.out.println(wallet.getWalletId());
        return Result.success(wallet.getWalletId());
    }

    //根据id查询余额和冻结余额
    @PostMapping("/selectYe/{walletId}")
    public Result<?> selectYe(@PathVariable String walletId){
        LambdaQueryWrapper<Wallet> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Wallet::getWalletId,walletId);
        Wallet one = walletService.getOne(wrapper);
        return Result.success(one);
    }

    //根据钱包id更新
    @PostMapping("/updateById")
    public Result<?> updateById(@RequestBody Wallet wallet){
        walletService.updateById(wallet);
        return Result.success();
    }
}

package com.hua.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hua.handler.Result;
import com.hua.pojo.User;
import com.hua.pojo.Withdrawal;
import com.hua.service.UserService;
import com.hua.service.WithdrawalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/withdrawal")
public class WithdrawalController {

    @Resource
    private WithdrawalService withdrawalService;

    @Autowired
    private UserService userService;

    @GetMapping
    //Get为查询
    public Result<?> withdrawalFindPage(@RequestParam(defaultValue = "1") Integer pageNum,
                                  @RequestParam(defaultValue = "10") Integer pageSize,
                                  @RequestParam(defaultValue = "") String search){
        LambdaQueryWrapper<Withdrawal> wrapper= Wrappers.<Withdrawal>lambdaQuery();
        if (StrUtil.isNotBlank(search)){
            wrapper.like(Withdrawal::getWithdrawalMoney,search);
        }
        Page<Withdrawal> page = withdrawalService.page(new Page<>(pageNum, pageSize), wrapper);
        //获取Withdrawal表中的一条数据
        List<Withdrawal> list = page.getRecords();
        //list是数据，item就和for循环中的i一样
        list.forEach(item -> {
            //getNickName方法传入一个参数，钱包id
            String nickName = userService.getNickName(item.getWalletId());
            //nickName替换WalletId
            item.setWalletId(nickName);
        });
        return Result.success(page);
    }

    @PutMapping
    //put为更新
    public Result<?> withdrawalUpdate(@RequestBody Withdrawal withdrawal ){
        withdrawalService.updateById(withdrawal);
        return Result.success();
    }

    //新增一个提现表的数据
    @PostMapping
    public Result<?> addWallet(@RequestBody Withdrawal withdrawal){
        withdrawalService.save(withdrawal);
        LambdaQueryWrapper<Withdrawal> wrapper = new LambdaQueryWrapper<>();
        wrapper.orderByDesc(Withdrawal::getCreateTime);
        List<Withdrawal> list = withdrawalService.list(wrapper);
        List<Withdrawal> withdrawals = list.subList(0, 1);
        return Result.success(withdrawals);
    }


    @DeleteMapping
    //Delete为删除，removeByIds为批量删除
    public Result<?>WithdrawalDelete(@RequestBody Set<String> ids){
        withdrawalService.removeByIds(ids);
        return Result.success();
    }
    //根据提现审核id查询提现状态
    @PostMapping("/selectStatus/{withdrawalId}")
    public Result<?> selectStatus(@PathVariable String withdrawalId){
        LambdaQueryWrapper<Withdrawal> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Withdrawal::getWithdrawalId,withdrawalId);
        Withdrawal one = withdrawalService.getOne(wrapper);
        return Result.success(one.getStatus());
    }

    //查询所有提现审核数据,看所有数据中是否有status=1的数据，有就代表有提现还在审核中
    @PostMapping("/selectWithdrawal")
    public Result<?> selectWithdrawal(){
        LambdaQueryWrapper<Withdrawal> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Withdrawal::getStatus,1);
        List<Withdrawal> list = withdrawalService.list(wrapper);
        return Result.success(list);
    }
}

package com.hua.handler;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
//统一返回结果的类
@Data
@AllArgsConstructor
public class Result <T>{
    private String code;
    private String msg;
    private String type;
    private T data;
    public Result(){}
    public Result(T data){this.data=data;}
    public static Result success(){
        Result result = new Result<>();
        result.setCode("0");
        result.setMsg("成功");
        return result;
    }
    public static <T> Result<T> success(T data){
        Result<T> result = new Result<T>(data);
        result.setCode("0");
        result.setMsg("成功");
        return result;
    }
    public static Result error(String code,String msg){
        Result result=new Result();
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
//    //登录注册
//    public static <T> Result<T> success(T data,String type){
//        Result<T> result = new Result<T>(data);
//        result.setCode("0");
//        result.setMsg("登录成功！");
//        return result;
//    }
//    //code状态码，msg信息，type颜色
//    public static Result error(String code,String msg,String type){
//        Result result=new Result();
//        result.setType(type);
//        result.setCode(code);
//        result.setMsg(msg);
//        return result;
//    }
}

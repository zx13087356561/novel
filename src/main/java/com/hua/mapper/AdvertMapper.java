package com.hua.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hua.pojo.Advert;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AdvertMapper extends BaseMapper<Advert> {
}
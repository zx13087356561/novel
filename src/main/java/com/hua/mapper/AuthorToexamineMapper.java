package com.hua.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hua.pojo.AuthorToexamine;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AuthorToexamineMapper extends BaseMapper<AuthorToexamine> {
}
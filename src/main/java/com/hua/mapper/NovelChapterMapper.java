package com.hua.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hua.pojo.NovelChapter;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface NovelChapterMapper extends BaseMapper<NovelChapter> {
}
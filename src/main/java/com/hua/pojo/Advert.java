package com.hua.pojo;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "advert")
public class Advert {
    /**
     * 广告ID
     */
    @TableId(value = "advert_id", type = IdType.ASSIGN_UUID)
    private String advertId;

    /**
     * 广告图片路径
     */
    @TableField(value = "advert_picture")
    private String advertPicture;

    /**
     * 广告是否展出状态(0代表不展出，1代表展出)
     */
    @TableField(value = "`status`")
    private Integer status;

    /**
     * 逻辑删除
     */
    @TableField(value = "is_deleted")
    @TableLogic
    private Integer isDeleted;

    /**
     * 字段创建时间
     */
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 字段修改时间
     */
    @TableField(value = "update_time",fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
}
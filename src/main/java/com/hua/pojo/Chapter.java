package com.hua.pojo;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "chapter")
public class Chapter {
    /**
     * 章节ID
     */
    @TableId(value = "chapter_id", type = IdType.ASSIGN_UUID)
    private String chapterId;

    /**
     * 章节名称 
     */
    @TableField(value = "chapter_name")
    private String chapterName;

    /**
     * 小说id
     */
    @TableField(value = "novel_id")
    private String novelId;

    /**
     * 章节序号
     */
    @TableField(value = "chapter_index")
    private Integer chapterIndex;

    /**
     * 具体写在章节里面的内容(文字)
     */
    @TableField(value = "chapter_content")
    private String  chapterContent;

    /**
     * 逻辑删除
     */
    @TableField(value = "is_deleted")
    @TableLogic
    private Integer isDeleted;

    /**
     * 字段创建时间
     */
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 字段修改时间
     */
    @TableField(value = "update_time",fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
}
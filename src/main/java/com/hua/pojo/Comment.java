package com.hua.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "`comment`")
public class Comment {
    /**
     * 评论ID
     */
    @TableId(type = IdType.ASSIGN_UUID)
    private String commentId;

    /**
     * 用户ID
     */
    @TableField(value = "user_id")
    private String userId;

    /**
     * 小说ID
     */
    @TableField(value = "novel_id")
    private String novelId;

    /**
     * 评价内容(具体文字)
     */
    @TableField(value = "comment_content")
    private String commentContent;


    /**
     * 逻辑删除(0为未删除，1为删除)
     */
    @TableField(value = "is_deleted")
    @TableLogic
    private Integer isDeleted;

    /**
     * 字段创建时间
     */
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 字段修改时间
     */
    @TableField(value = "update_time",fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
}
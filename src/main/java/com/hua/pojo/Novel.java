package com.hua.pojo;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "novel")
public class Novel {
    /**
     * 小说ID
     */
    @TableId(value = "novel_id", type = IdType.ASSIGN_UUID)
    private String novelId;

    /**
     * 小说名称
     */
    @TableField(value = "novel_name")
    private String novelName;

    /**
     * 小说点击量
     */
    @TableField(value = "novel_hits")
    private Integer novelHits;

    /**
     * 小说审核状态(0为审核未通过，1为审核通过)
     */
    @TableField(value = "`status`")
    private Integer status;

    /**
     * 小说简介
     */
    @TableField(value = "novel_title")
    private String novelTitle;

    /**
     * 分类ID
     */
    @TableField(value = "type_id")
    private String typeId;

    /**
     * 用户ID
     */
    @TableField(value = "user_id")
    private String userId;

    /**
     * 小说图片
     */
    @TableField(value = "novel_img")
    private String novelImg;

    /**
     * 逻辑删除(0为未删除，1为删除)
     */
    @TableField(value = "is_deleted")
    @TableLogic
    private Integer isDeleted;

    /**
     * 字段创建时间
     */
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 字段修改时间
     */
    @TableField(value = "update_time",fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


}
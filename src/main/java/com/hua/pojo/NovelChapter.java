package com.hua.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "novel_chapter")
public class NovelChapter {
    /**
     * 小说章节ID
     */
    @TableId(value = "novel_chapter_id", type = IdType.INPUT)
    private String novelChapterId;

    /**
     * 小说ID
     */
    @TableField(value = "novel_id")
    private String novelId;

    /**
     * 章节ID
     */
    @TableField(value = "chapter")
    private String chapter;

    /**
     * 逻辑删除
     */
    @TableField(value = "is_deleted")
    private Integer isDeleted;

    /**
     * 字段创建时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 字段修改时间
     */
    @TableField(value = "update_time")
    private Date updateTime;
}
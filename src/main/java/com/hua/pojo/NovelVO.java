package com.hua.pojo;

import lombok.Data;

@Data
public class NovelVO extends Novel{
    private String typeName;
    private String nickName;
}

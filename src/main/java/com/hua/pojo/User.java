package com.hua.pojo;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "`user`")
public class User {
    /**
     * 用户ID
     */
    @TableId(value = "user_id", type = IdType.ASSIGN_UUID)
    private String userId;

    /**
     * 用户名
     */
    @TableField(value = "user_name")
    private String userName;

    /**
     * 手机号码
     */
    @TableField(value = "phone")
    private String phone;

    /**
     * 头像地址
     */
    @TableField(value = "head_portrait")
    private String headPortrait;

    /**
     * 昵称
     */
    @TableField(value = "nick_name")
    private String nickName;


    /**
     * 密码
     */
    @TableField(value = "`password`")
    private String password;

    /**
     * 钱包ID
     */
    @TableField(value = "wallet_id")
    private String walletId;

    /**
     * 身份(0为普通读者，1为作者)
     */
    @TableField(value = "`identity`")
    private Integer identity;

    /**
     * 逻辑删除
     */
    @TableField(value = "is_deleted")
    @TableLogic
    private Integer isDeleted;

    /**
     * 字段创建时间
     */
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 字段修改时间
     */
    @TableField(value = "update_time",fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
}
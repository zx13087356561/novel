package com.hua.pojo;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "wallet")
public class Wallet {
    /**
     * 钱包ID
     */
    @TableId(value = "wallet_id", type = IdType.ASSIGN_UUID)
    private String walletId;

    /**
     * 余额
     */
    @TableField(value = "balance")
    private Long balance;

    /**
     * 冻结余额
     */
    @TableField(value = "frozen_balance")
    private Long frozenBalance;

    /**
     * 逻辑删除
     */
    @TableField(value = "is_deleted")
    @TableLogic
    private Integer isDeleted;

    /**
     * 字段创建时间
     */
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 字段修改时间
     */
    @TableField(value = "update_time",fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
}
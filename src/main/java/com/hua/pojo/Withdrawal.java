package com.hua.pojo;

import com.baomidou.mybatisplus.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "withdrawal")
public class Withdrawal {
    /**
     * 提现审核ID
     */
    @TableId(value = "withdrawal_id", type = IdType.ASSIGN_UUID)
    private String withdrawalId;

    /**
     * 钱包ID
     */
    @TableField(value = "wallet_id")
    private String walletId;

    /**
     * 提现金额
     */
    @TableField(value = "withdrawal_money")
    private Integer withdrawalMoney;

    /**
     * 提现审核状态(0代表未通过，1通过)
     */
    @TableField(value = "`status`")
    private Integer status;

    /**
     * 逻辑删除
     */
    @TableField(value = "is_deleted")
    @TableLogic
    private Integer isDeleted;

    /**
     * 字段创建时间
     */
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 字段修改时间
     */
    @TableField(value = "update_time",fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
}
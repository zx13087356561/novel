package com.hua.service;

import com.hua.pojo.Novel;
import com.baomidou.mybatisplus.extension.service.IService;
public interface NovelService extends IService<Novel>{

    Novel selectNovelData(String novelId);

    String getSelectNovelName(String novelId);
}

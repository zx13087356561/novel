package com.hua.service;

import com.hua.pojo.User;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hua.pojo.Withdrawal;

public interface UserService extends IService<User>{

    String getNickName(String walletId);

    String getSelectUserNickName(String userId);
}

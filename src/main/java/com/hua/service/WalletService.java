package com.hua.service;

import com.hua.pojo.Wallet;
import com.baomidou.mybatisplus.extension.service.IService;
public interface WalletService extends IService<Wallet>{


    Long selectBalance(String walletId);
}

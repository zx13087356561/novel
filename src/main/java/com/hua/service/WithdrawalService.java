package com.hua.service;

import com.hua.pojo.Withdrawal;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface WithdrawalService extends IService<Withdrawal>{

}

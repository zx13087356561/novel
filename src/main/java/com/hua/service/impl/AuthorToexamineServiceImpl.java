package com.hua.service.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hua.mapper.AuthorToexamineMapper;
import com.hua.pojo.AuthorToexamine;
import com.hua.service.AuthorToexamineService;
@Service
public class AuthorToexamineServiceImpl extends ServiceImpl<AuthorToexamineMapper, AuthorToexamine> implements AuthorToexamineService{

}

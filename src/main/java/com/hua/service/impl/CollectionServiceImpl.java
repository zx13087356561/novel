package com.hua.service.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hua.pojo.Collection;
import com.hua.mapper.CollectionMapper;
import com.hua.service.CollectionService;
@Service
public class CollectionServiceImpl extends ServiceImpl<CollectionMapper, Collection> implements CollectionService{

}

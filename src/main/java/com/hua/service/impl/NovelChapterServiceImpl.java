package com.hua.service.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hua.mapper.NovelChapterMapper;
import com.hua.pojo.NovelChapter;
import com.hua.service.NovelChapterService;
@Service
public class NovelChapterServiceImpl extends ServiceImpl<NovelChapterMapper, NovelChapter> implements NovelChapterService{

}

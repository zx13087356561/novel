package com.hua.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.hua.handler.Result;
import com.hua.mapper.TypeMapper;
import com.hua.mapper.UserMapper;
import com.hua.pojo.Type;
import com.hua.pojo.User;
import com.hua.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hua.mapper.NovelMapper;
import com.hua.pojo.Novel;
import com.hua.service.NovelService;
@Service
public class NovelServiceImpl extends ServiceImpl<NovelMapper, Novel> implements NovelService{

    @Resource
    private UserService userService;

    @Override
    //根据小说id查询小说内容
    public Novel selectNovelData(String novelId) {
        LambdaQueryWrapper<Novel> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Novel::getNovelId,novelId);
        Novel one = getOne(wrapper);
        String userId = one.getUserId();
        LambdaQueryWrapper<User> wrapper1 = new LambdaQueryWrapper<>();
        wrapper1.eq(User::getUserId,userId);
        String nickName = userService.getOne(wrapper1).getNickName();
        one.setUserId(nickName);
        return one;
    }

    @Override
    public String getSelectNovelName(String novelId) {
        LambdaQueryWrapper<Novel> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Novel::getNovelId,novelId);
        Novel one = getOne(wrapper);
        String novelName = one.getNovelName();
        return novelName;
    }

}

package com.hua.service.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hua.pojo.Type;
import com.hua.mapper.TypeMapper;
import com.hua.service.TypeService;
@Service
public class TypeServiceImpl extends ServiceImpl<TypeMapper, Type> implements TypeService{

}

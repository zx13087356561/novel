package com.hua.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.hua.pojo.Comment;
import com.hua.pojo.Withdrawal;
import com.hua.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hua.mapper.UserMapper;
import com.hua.pojo.User;
import com.hua.service.UserService;
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService{

    @Resource
    private CommentService commentService;

    @Override
    public String getNickName(String walletId) {
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(User::getWalletId,walletId);
        String userName = getOne(wrapper).getNickName();
        return userName;
    }

    @Override
    public String getSelectUserNickName(String userId) {
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(User::getUserId,userId);
        User one = getOne(wrapper);
        String nickName = one.getNickName();
        return nickName;
    }


}

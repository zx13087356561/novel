package com.hua.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hua.pojo.Wallet;
import com.hua.mapper.WalletMapper;
import com.hua.service.WalletService;

import java.util.List;

@Service
public class WalletServiceImpl extends ServiceImpl<WalletMapper, Wallet> implements WalletService{


    @Override
    public Long selectBalance(String walletId) {
        //获取到Wallet中的数据
        LambdaQueryWrapper<Wallet> wrapper = new LambdaQueryWrapper<>();
        //把wallet表中的walletId，和传入的user表中的walletId中的值做对比
        wrapper.eq(Wallet::getWalletId,walletId);
        //查询wallet表中的这条数据，取出其中的banlance(余额)，进行返回
        Long balance = getOne(wrapper).getBalance();
        return balance;
    }
}

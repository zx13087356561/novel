package com.hua;

import com.hua.mapper.AdvertMapper;
import com.hua.mapper.TypeMapper;
import com.hua.pojo.Advert;
import com.hua.pojo.Type;
import com.hua.service.TypeService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class NovelApplicationTests {

    @Autowired
    private TypeMapper typeMapper;

    @Autowired
    private TypeService typeService;

    @Test
    void contextLoads() {
//        只是一个查询的例子罢了
        List<Type> adverts = typeMapper.selectList(null);
        adverts.forEach(System.out::println);
        System.out.println("我到了只是没数据罢！");
    }


    @Test
    void a(){
        Type fack = Type.builder().typeId("62838356228aeb994d1745581aa5b060").typeName("旭日东升").build();
        typeService.updateById(fack);
//        typeService.removeById("26d41851177d0d5024e3303f03f7173a");
//        Type deleted = Type.builder().typeId("26d41851177d0d5024e3303f03f7173a").isDeleted(0).typeName("华子到此四游").build();
//        typeService.updateById(deleted);
//        Type aa = Type.builder().typeName("张旭~").typeId(null).build();
//        typeService.save(aa);
    }

}
